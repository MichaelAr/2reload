matrix = [ 
  [:rock, 	:rock, 		:draw],
	[:scissors, :scissors,	:draw],
	[:paper,	:paper,		:draw],

  [:rock,		:scissors,	:first_win],
	[:rock,		:paper,		:second_win],

  [:scissors,	:rock,		:second_win],
	[:scissors,	:paper,		:first_win],

  [:paper,	:rock,		:first_win],
	[:paper,	:scissors,	:second_win]
]

print '(R)ock (S)cissors (P)aper: '
s  = gets.strip.capitalize

if s == 'R'
  user_choice = :rock
elsif s == 'S'
  user_choice = :scissors
elsif s == 'P'
  user_choice = :paper
else 
  print 'Wrong command, i dont understand you, try againg!'
  exit
end

arr = [ :rock, :paper, :scissors]
computer_choice = arr[rand(0..2)]

puts "User choice: #{user_choice}"
puts "Computer choice: #{computer_choice}"

matrix.each do |item|
  if item[0] == user_choice && item[1] == computer_choice
    if item[2] == :draw 
      puts 'Its a Draw!!!'
    elsif item[2] == :first_win
      puts 'User Wins!!!'
    elsif item[2] == :second_win
    end
    exit
  end
end