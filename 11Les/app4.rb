matrix = [ 
  [:rock, 	:rock, 		:draw],
	[:scissors, :scissors,	:draw],
	[:paper,	:paper,		:draw],

  [:rock,		:scissors,	:first_win],
	[:rock,		:paper,		:second_win],

  [:scissors,	:rock,		:second_win],
	[:scissors,	:paper,		:first_win],

  [:paper,	:rock,		:first_win],
	[:paper,	:scissors,	:second_win]
]


print 'Press (R)ock (S)cissors (P)aper: '
s = gets.strip.capitalize

if s == 'R'
  user_choice = :rock
elsif s == 'S'
  user_choice = :scissors
elsif s == 'P'
  user_choice = :paper
else 
  print 'I dont understand you, try again! '
  exit
end


arr = [:scissors, :rock, :paper]
computer_choice = arr[rand(0..2)]

puts "User Choice: #{user_choice}"
puts "Computer Choice: #{computer_choice}"


matrix.each do |item|
  if item[0] == user_choice && item[1] == computer_choice
    if item[2] == :draw
      print 'Its a draw!'
    elsif item[2] == :first_win 
      print 'User Win!'
    elsif item[2] == :second_win
      print 'Computer Win!'
    end
  end
end

