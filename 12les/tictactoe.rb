@table = [
         #X0   #X1   #X2
        ["-", "-", "-"], #Y0
        ["-", "-", "-"], #Y1
        ["-", "-", "-"], #Y2
]



def print_table op
    puts " C1 C2 C3"
    op.each_with_index do |x, ind|
        puts "L#{ind+1} #{x[0]} #{x[1]} #{x[2]}"
    end
end

def is_number? obj
    obj.to_s == obj.to_i.to_s
end

def computer_turn comp
    puts "Computer turn! "
    a = rand(0..2)
    b = rand(0..3)
    until @table[a][b] !=@user && @table[a][b] == "-"
    a = rand(0..2)
    b = rand(0..3)
    end
    @table[a][b] = comp
    print_table @table
    matrix @table
end


def user_turn user
    puts "User turn! "
    puts "What column? "
    c = gets.strip.to_i
    puts "What line? "
    d = gets.strip.to_i
    @table[c-1][d-1] = @user

    print_table @table
    matrix @table
end

def matrix opt
    if opt[0][0] == opt[0][1] && opt[0][1] == opt[0][2] && opt[0][0] != "-"
        puts "#{opt[0][0]} Winner! "
        exit
    elsif opt[0][0] == opt[1][0] && opt[1][0] == opt[2][0] && opt[0][0] != "-"
        puts "#{opt[0][0]} Winner! "
        exit
    elsif opt[2][0] == opt[2][1] && opt[2][1] == opt[2][2] && opt[2][0] != "-"
            puts "#{opt[2][0]} Winner! "
            exit
    elsif opt[0][1] == opt[1][1] && opt[1][1] == opt[2][1] && opt[0][1] != "-"
                puts "#{opt[0][1]} Winner!"
                exit
    elsif opt[0][2] == opt[1][2] && opt[1][2] == opt[2][2] && opt[0][2] != "-"
                    puts "#{opt[0][2]} Winner! "
                    exit
    elsif opt[1][0] == opt[1][1] && opt[1][1] == opt[1][2] && opt[1][0] != "-"
                        puts "#{opt[1][0]} Winner! "
                        exit
    elsif opt[0][0] == opt[1][1] && opt [1][1] == opt[2][2] && opt[0][0] != "-"
                            puts "#{opt[0][0]} Winner! "
                            exit
    elsif opt[0][2] == opt[1][1] && opt[1][1] == opt[2][0] && opt[0][2] !="-"
                                puts "#{opt[0][2]} Winner! "
                                exit
    end

draw = 0 
opt.each_with_index do |x,ind|
    x.each do |comp|
        if comp == "-"
            break
        else
            draw = draw +1
        end
    end
    if draw == 9 
        puts "Its draw! "
        exit
    end
end
end

side = ["X", "O"]
@user = side[rand(0..1)]
puts "==========================================\nYour side\n========================================== #{@user} \nX goes first!"
loop do 
print_table @table

if @user == "X" 
    user_turn "X"
    computer_turn "O"
else user_turn "O"
    computer_turn "X"
end

end
                        


