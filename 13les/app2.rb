@hh = {}

def add_person(options)
  #add to hash
    puts 'Already exists!' if @hh[options[:name]]

  @hh[options[:name]] = options[:age]
end

def show_hash
  #showHash
  @hh.keys.each do |key|
    age = @hh[key]
    puts "Name: #{key}, age: #{age}"
  end
end

loop do 
  #enter to stop

  print "Enter name: "
  name = gets.to_s.strip.capitalize

  print "Enter age: "
  age = gets.to_i

  options =  {name: name, age: age}
  add_person options
  if name == ""
    show_hash
    break
  end
  
end
