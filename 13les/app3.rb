require 'pry'

def show_book(book)
  # виводить на екран книгу
  puts '======================================'
  book.keys.each do |key|
    age = book[key]
    puts "Name: #{key}, age: #{age}"
  end
  puts '======================================'

end

book1 = { 'Mike' => 65, 'Ned' => 33, 'Ted' => 24, 'Nen' => 88 }
# binding.pry
show_book book1

book2 = { 'Walt' => 50, 'Gose' => 66, 'Jess' => 44 }
show_book book2

book = book1.merge(book2)

show_book book


