class Airport
  attr_reader :name, :planes

  def initialize(name)
    @name = name
    @planes = []
  end

  def add_plane(plane)
    planes << plane
  end
end

class Plane 
  attr_reader :model

  def initialize(model)
    @model = model
  end
end

class Country
  attr_reader :name, :city

  def initialize(name)
    @name = name
    @city = []
  end

  def add_airport(airports)
    @city << airports
  end
end

airports = []

airport1 = Airport.new('MegaAiro')
airport2 = Airport.new('AiroHappy')
airport3 = Airport.new('PushTheTempo')

plane1 = Plane.new('FirstPlane')
plane2 = Plane.new('SecondPlane')
plane3 = Plane.new('ThirdPlane')

airport1.add_plane plane1
airport2.add_plane plane2
airport3.add_plane plane3

airports << airport1
airports << airport2
airports << airport3

city = []

country1 = Country.new 'USA'
country1.add_airport airport1
country1.add_airport airport2
country1.add_airport airport3
country2 = Country.new 'Ukraine'
country2.add_airport airport1
city << country1
city << country2

city.each do |countrys|
  puts countrys.name
  airports.each do |airport|
    puts airport.name
      airport.planes.each do |plane|
        puts plane.model
      end
  end
end

airports.each do |airport|
  puts airport.name
  airport.planes.each do |plane|
    puts plane.model
  end
end
