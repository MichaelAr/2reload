class Animal 
  def initialize(name)
    @name = name
  end

  def run
    puts "#{@name} is running..."
  end

  def jump
    puts "#{@name} is jumping..."
  end
end

class Dog < Animal
  def initialize
    super 'dog'
  end

  def bark
    puts "#{@name} Woof - woof!"
  end
end

class Cat < Animal
  def initialize
    super 'cat'
  end

  def say_meoew
    puts "#{@name} Meeeeooooow!"
  end
end



cat = Cat.new
cat.run
cat.say_meoew

dog = Dog.new
dog.bark
dog.jump