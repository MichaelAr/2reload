class Album 
  attr_accessor :name, :songs

  def initialize(name)
    @name = name 
    @songs = []
end

  def add_song(song)
    @songs << song
  end
end

class Song
  attr_accessor :name, :duration

  def initialize(name, duration)
    @name = name
    @duration = duration
  end
end

albums = []
album1 = Album.new 'And Justice for All'
album2 = Album.new 'Master of Puppets'

albums << album1
albums << album2

songs = []
song1 = Song.new 'Blackened', '5:31'
song2 = Song.new 'And Justice for All', '9:46'
song3 = Song.new 'Eye of the Beholder', '6:26'
song4 = Song.new 'Battery', '5:26'
song5 = Song.new 'Master of Puppets', '8:35'
song6 = Song.new 'The Thing That Should Not Be', '6:37'

album1.add_song song1
album1.add_song song2
album1.add_song song3 

album2.add_song song4
album2.add_song song5
album2.add_song song6

albums.each do |albm|
  puts albm.name
  albm.songs.each do|metall|
    puts metall.name
    puts metall.duration
  end
end






