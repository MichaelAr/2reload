class Album
  attr_accessor :name, :songs

  def initialize(name)
    @name = name
    @songs = []
  end

  def add_song(song)
    @songs << song
  end
end

class Song
  attr_accessor :name, :duration

  def initialize(name)
    @name = name
    @duration = 640
  end
end

albums = []
album1 = Album.new 'And Justice for All'
album2 = Album.new 'Master of Puppets'

albums << album1
albums << album2

@songs = []
song1 = Song.new 'Blackened'
song2 = Song.new 'And Justice for All'
song3 = Song.new 'Eye of the Beholder'
song4 = Song.new 'Battery'
song5 = Song.new 'Master of Puppets'
song6 = Song.new 'The Thing That Should Not Be'

album1.add_song song1
album1.add_song song2
album1.add_song song3

album2.add_song song4
album2.add_song song5
album2.add_song song6

albums.each do |albm|
  puts albm.name
  albm.songs.each do |metall|
    puts metall.name
  end
end



