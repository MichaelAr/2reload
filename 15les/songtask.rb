class Album
  attr_accessor :name, :songs

  def initialize(name)
    @name = name
    @songs = []
  end

  def add_song(song)
    @songs << song
  end
end

class Song
  attr_accessor :name, :duration

  def initialize(name, duration)
    @name = name
    @duration = duration
  end
end

album1 = Album.new('Iron Horse')

song1 = Song.new('Fuel', 7)
song2 = Song.new('Enter Sandman', 8)

album1.add_song song1
album1.add_song song2

puts album1.name
puts album1.songs[0].name
puts album1.songs[1].name

