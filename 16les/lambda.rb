# frozen_string_literal: false

say_hi = -> { puts 'Hi' }
say_bye = -> { puts 'Bye' }

week = [say_hi, say_hi, say_hi, say_hi, say_hi, say_bye, say_bye]

week.each(& :call)
