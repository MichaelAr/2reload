add_10 = lambda { |x| x + 10 }
add_20 = lambda { |x| x + 20 }
sub_5 = lambda { |x| x - 5 }

balance = 1000

# if < 300 add 10
# if < 600 add 20
# if > 600 sub 5
hh = { 111 => add_10, 222 => add_10, 333 => add_20, 444 => add_20, 666 => sub_5, 777 => sub_5, 888 => sub_5, 999 => sub_5 }

1000.times do
  x = rand(100..999)

  puts "Combination: #{x}"

  if hh[x]
    f = hh[x]
    balance = f.call balance
    puts 'Lambda called'
  else
    balance = sub_5.call balance
  end

    puts "Your Balance: #{balance}"
end