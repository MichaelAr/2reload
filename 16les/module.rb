module Humans
  class Manager
    def say_hi
      puts 'Hi'
    end
  end

  class Hipster
    def say_hi
      puts 'Hiii, yo!'
    end
  end

  class JessiePinkman
    def say_hi
      puts 'Hi, bitch!'
    end
  end
end

hipster1 = Humans::Hipster.new
hipster1.say_hi
