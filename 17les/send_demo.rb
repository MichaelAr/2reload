def mm
  puts 'Hello'
end

send :mm

def aa(args)
  puts args
end

send :aa, 1

def qq(hhh)
  puts hhh.inspect
end

send 'qq', x: 1, y: 2

class Something
  attr_accessor :name

  def initialize
    send('name=', 'Mike') # = @name = 'Mike'
  end
end

s = Something.new

puts s.inspect
puts s.name


class Some2thing
  attr_accessor :x, :y

  def initialize hash
    hash.each do |key, value|
      send("#{key}=", value)
    end
  end
end

  s = Some2thing.new(x: 11, y: 22)
  puts s.x