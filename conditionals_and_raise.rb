# number >= 10
# (number % 2) == 0 -> even?

def valid_number(number)
  number >= 10 && number.even?
end

puts valid_number(10)
# puts valid_number(1)
# puts valid_number('cat')

 # raise ArgumentError, 'STOP!!!' unless number.is_a?(Integer)
  # return false unless number >= 10

  # number.even? ? true : false