class Robot
  def initialize(name, production_number)
    @name = name
    @production_number = production_number
  end

  def say
    puts "Hello, my name #{@name}"
  end

  def self.produce(name, count)
    0.upto(count - 1).map do |item_number|
      Robot.new(name, "#{name}-ITEM-#{item_number}")
    end
  end
end

Robot.produce('r2d2', 15)