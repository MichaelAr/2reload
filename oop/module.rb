class Parent
  def something
  end
end

module Mixin
  def shared_between_unrelated_classes
  end
end

class Children < Parent
  include Mixin
end

class A
  include Mixin
end