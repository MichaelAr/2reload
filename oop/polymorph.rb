class Student
  def do_work
    puts 'Doing homework...'
  end
end

class Teacher
  def do_work
    puts 'Checking homework'
  end
end

class HeadTeacher
  def make_everybody_work(somebody)
    somebody.do_work
  end
end

student = Student.new
fizruk = Teacher.new
petrivna = HeadTeacher.new

petrivna.make_everybody_work(student)
petrivna.make_everybody_work(fizruk)