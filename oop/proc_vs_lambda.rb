# lam_greeter = ->(name) {puts "Hello, #{name}"}

# lam_greeter.call('#Pivorak Student')

# lam_greeter.call
# wrong number of arguments (given 0, expected 1)

# lam_greeter.call('#Pivorak Student', 18)
# wrong number of arguments (given 2, expected 1) (ArgumentError)

# proc_greeter = proc { |name| puts "Hello, #{name}"}
# proc_greeter.call('#Pivorak Student')

# proc_greeter.call
# Hello, 
# proc_greeter.call('#Pivorak Student', 18)
# Hello, #Pivorak Student

def lambda_greeter
  brake = -> { return }
  puts 'Hello, #Pivorak Student'
  brake.call
  puts 'You will be professoinal soon'
end

puts lambda_greeter

# Hello, #Pivorak Student
# You will be professoinal soon

def proc_greeter
  brake = proc { return }
  puts 'Hello, #Pvorak Student'
  brake.call
  puts ' You will be professional soon'
end

puts proc_greeter

# Hello, #Pvorak Student

