module Mixin
  def test_super(one)
    super(one, 55)
  end
end

class Parent
  def test_super(one, two)
    puts "One: #{one}, Two: #{two}"
  end
end

class Children < Parent
  def test_super(one, two)
    puts 'I know better how to output those arguments'
    super(one, 9)
  end
end

child = Children.new
child.test_super(1, 5)