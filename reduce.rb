collection = [2, 3, 'cat', 88 ]

collection.reduce([]) do |acc, element|
  element.is_a?(Integer) ? (acc << element) : acc
end
