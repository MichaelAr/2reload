
FOOD = 1000

def dog_food
  puts 'Введіть кількість днів: '
  days = gets.chomp

  begin
    grams = FOOD/days.to_i
  rescue ZeroDivisionError
    puts 'Не може бути нулем'
  else
    puts "Якщо давати собаці #{grams} грам"
    puts "то вистачить на #{days} днів "
  end
end

dog_food
